import unittest
import calc
from errors import InvalidOperation
from math import factorial

class TestCalculations(unittest.TestCase):

    def test_no_operation(self):
        self.assertRaises(InvalidOperation, calc.process_json, {'values':[1, 2, 3]})

    def test_no_values(self):
        self.assertRaises(InvalidOperation, calc.process_json, {'operation':'add'})

    def test_invalid_operation(self):
        self.assertRaises(InvalidOperation, calc.process_json, {'operation':'xyzzy', 'values':[1, 2, 3]})

    def test_expected_sum(self):
        _v = {'operation': 'add', 'values': [1, 2, 3]}
        result = calc.process_json(_v)
        self.assertEqual(6, result)

    def test_expected_mul(self):
        _v = {'operation': 'multiply', 'values': [7, 6]}
        result = calc.process_json(_v)
        self.assertEqual(42, result)

    def test_single_add(self):
        _v = {'operation': 'add', 'values': 5}
        result = calc.process_json(_v)
        self.assertEqual(5, result)

    def test_single_mul(self):
        _v = {'operation': 'add', 'values':12345}
        result = calc.process_json(_v)
        self.assertEqual(12345, result)

    def test_factorial(self):
        range_ten = range(1, 10)   # list from 1 to 9
        _v = {'operation': 'multiply', 'values': range_ten}
        c_result = calc.process_json(_v)
        f_result = factorial(9)
        self.assertEqual(f_result, c_result)
