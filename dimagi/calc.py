from errors import InvalidOperation

def process_json(container):
    """Takes a dict containing an operation and a values-list (or single value). Returns the result of that operation."""
    if 'operation' not in container:
        raise InvalidOperation("Operation missing.")
    if 'values' not in container:
        raise InvalidOperation("Values to operate on are missing.")
    if container['operation'] == 'add':
        return op_add(container['values'])
    if container['operation'] == 'multiply':
        return op_mul(container['values'])
    raise InvalidOperation("Operation not supported", status_code=403)

def op_add(data):
    """Add all elements and return their sum. Can accept single number or a list of numbers. One single number returns that number."""
    if isinstance(data, list):
        return sum(data)
    return data

def calc_mul(data):
    """Multiply all elements in an iterable, similar to factorial but for arbitrary lists."""
    total = 1
    for d in data:
        total = total * d
    return total

def op_mul(data):
    """Multiply all elements together and return that value. Can accept single number or a list of numbers. One single number returns that number."""
    if isinstance(data, list):
        return calc_mul(data)
    return data