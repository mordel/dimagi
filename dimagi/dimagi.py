from flask import Flask, request, jsonify
from calc import process_json
from errors import InvalidOperation

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST', ])
def math_engine():
    data = request.get_json()
    result = process_json(data)
    return jsonify(result=result)

@app.errorhandler(InvalidOperation)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

if __name__ == '__main__':
    app.run(debug=True)
