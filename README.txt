+ Dimagi Calculator

Simple add or multiply calculator web service, written in Flask.

To install, you can run `pip install git+https://mordel@bitbucket.org/mordel/dimagi.git`

After that, to execute the web service run `python -m dimagi.dimagi` and the web server will start up.
