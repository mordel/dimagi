class gunicorn_process ($app, $path) {
  package { 'supervisor' :
    ensure => installed,
  } 
  file { '/etc/supervisor/conf.d/dimagi' :
    ensure => present,
    content => template('gunicorn_process.erb')
  }
}

node default {

  class { 'python':
    version    => 'system',
    dev        => true,
    virtualenv => true,
    gunicorn   => true,
  }

  python::virtualenv { '/opt/dimagi/env':
    ensure       => present,
    version      => 'system',
    requirements => '/opt/dimagi/requirements.txt',
    systempkgs   => true,
    distribute   => false,
    cwd          => '/opt/dimagi/env',
    timeout      => 0,
  }

  class { 'gunicorn_process':
    app => 'dimagi.dimagi:app',
    path => '/opt/dimagi'
  } 

}
