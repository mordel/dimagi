class gunicorn_process ($app, $path) {
  package { 'supervisor' :
    ensure => installed,
  }
  file { '/etc/supervisor.d/dimagi' :
    ensure => present,
    content => template('gunicorn_process.erb')
  }
}
