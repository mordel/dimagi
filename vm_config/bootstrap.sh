apt-get install -y git;

if [ ! -d "/etc/puppet/modules" ]; then
	mkdir -p /etc/puppet/modules
	git clone git://github.com/stankevich/puppet-python.git /etc/puppet/modules/python
fi

if [ ! -d "/opt/dimagi" ]; then
	mkdir -p /opt/dimagi
	git clone https://mordel@bitbucket.org/mordel/dimagi.git /opt/dimagi
else
	cd /opt/dimagi
	git pull
fi
