from distutils.core import setup

setup(
    name='Dimagi',
    version='0.1.0',
    author='Stephen T. Weber',
    author_email='stephen.t.weber@gmail.com',
    packages=['dimagi',],
    scripts=[],
    url='http://pypi.python.org/pypi/Dimagi/',
    license='LICENSE.txt',
    description='Simple calculator web service.',
    long_description=open('README.txt').read(),
    install_requires=['Fabric', 'Flask', 'nose'],
)